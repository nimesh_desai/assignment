//
//  Router.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Alamofire

enum Router : URLRequestConvertible {
    
    case Homescreen_Collection_Urls
    case Homescreen_Collection(String)
    case Image(String, String)
    case Movie(UInt)
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .Homescreen_Collection_Urls:
            return Constants.API.URL
            
        case .Homescreen_Collection(let string):
            return string
            
        case .Image(let width, let url):
            return width + url
            
        case .Movie(let id):
            return String(id)
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var url: URL
        var urlRequest: URLRequest
        
        switch (self) {
        
        case .Homescreen_Collection_Urls:
            url = try Router.Constants.API.URL.asURL()
            urlRequest = URLRequest(url: url)
            
        case .Homescreen_Collection(_):
            url = try Router.Constants.API.baseURL.asURL()
            urlRequest = URLRequest(url: url.appendingPathComponent(path))
            self.updateRequestWithHeaders(&urlRequest)
            
        case .Image(_, _):
            url = try (Constants.API.imageBaseURL + path).asURL()
            urlRequest = URLRequest(url: url)
            self.updateRequestWithHeaders(&urlRequest)
            
        case .Movie(_):
            url = try Router.Constants.API.baseURL.asURL()
            urlRequest = URLRequest(url: url.appendingPathComponent(path))
            self.updateRequestWithHeaders(&urlRequest)
        }
        
        urlRequest.httpMethod = method.rawValue

        return urlRequest
    }
    
    private func updateRequestWithHeaders(_ urlRequest: inout URLRequest) {
        urlRequest.setValue(Constants.Headers.Content_Type_Value,
                            forHTTPHeaderField: Constants.Headers.Content_Type_Key)
        urlRequest.setValue(Constants.Headers.Authorization_Value,
                            forHTTPHeaderField: Constants.Headers.Authorization_Key)
    }
}
