//
//  ServiceLayer.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation
import Alamofire

class ServiceLayer {
    
    // Private
    private var isNetworkReachable: Bool {
        get {
            if let manager = NetworkReachabilityManager() {
                return manager.isReachable
            }
            
            return false
        }
    }
    
    //Public
    
    // Fetch Home Screen Collection URLs
    
    // I have decided to use Alamofire to display my skills using Cocoapods and Alamofire.
    // Data and image fetch methods can also be easily implemented using URLSession, something along the lines as shown below:
    
    public func fetch_homescreen_collection_urls_without_AF(_ completion: @escaping (Result<[HomeScreenCollection_SourceUrl], SevenPlusError>) -> Void) {
        do {
            let url: URLRequest = try Router.Homescreen_Collection_Urls.asURLRequest()

            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard error == nil else {
                    let urlError = error as? URLError
                    let error = SevenPlusError.getSevenPlusErrorForUrlError(urlError, Constants.Error.errorDomain)
                    completion(.failure(error))
                    return
                }

                guard let homescreenCollectionsData = data else {
                    let error = SevenPlusError.getDefaultError(Constants.Error.errorDomain)
                    completion(.failure(error))
                    return
                }

                guard let sourceUrls = HomeScreenCollection_SourceUrl_JSONSerializer.generateHomeScreenCollectionSourceUrlsFrom(homescreenCollectionsData) else {
                    let error = SevenPlusError.getDefaultError(Constants.Error.errorDomain)
                    completion(.failure(error))
                    
                    return
                }
                
                completion(.success(sourceUrls))
            }

            task.resume()
        } catch {
            let error = SevenPlusError.getDefaultError(Constants.Error.errorDomain)
            completion(.failure(error))
        }
    }
    
    public func fetch_homescreen_collection_urls(_ completion: @escaping (Result<[HomeScreenCollection_SourceUrl], SevenPlusError>) -> Void) {
        
        if self.isNetworkReachable {
            AF.request(Router.Homescreen_Collection_Urls).response(queue: DispatchQueue.global(qos: .background)) { (response) in
                
                switch response.result {
                
                case .success(_):
                    if let homescreenCollectionsData = response.value ?? nil {
                        if let collections = HomeScreenCollection_SourceUrl_JSONSerializer.generateHomeScreenCollectionSourceUrlsFrom(homescreenCollectionsData) {
                            completion(.success(collections))
                        } else {
                            //create Error obj - throw unable to parse data error
                            let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                            completion(.failure(error))
                            
                        }
                    } else {
                        //create Error obj - throw unable to parse data error
                        let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                        completion (.failure(error))
                    }
                    
                
                case .failure(let afError):
                    let error = SevenPlusError.getSevenPlusErrorForUrlError(afError.underlyingError as? URLError, Constants.Error.errorDomain)
                    completion (.failure(error))
                }
            }
        } else {
            //create Error obj - throw network error
            let error = SevenPlusError.getNetworkError(Constants.Error.errorDomain)
            completion (.failure(error))
        }
    }
    
    public func fetch_homescreen_collection(_ collectionUrl: String,
                                            _ title: String,
                                            _ completion: @escaping (Result<HomeScreenCollection, SevenPlusError>) -> Void) {
        
        if self.isNetworkReachable {
            AF.request(Router.Homescreen_Collection(collectionUrl)).response(queue: DispatchQueue.global(qos: .background)) { (response) in
                switch response.result {
                    
                case .success(_) :
                    
                    if let collectionData = response.value ?? nil {
                        if var collection = HomeScreenCollection_JSONSerializer.generateHomeScreenCollectionFrom(collectionData) {
                            collection.title = title
                            completion(.success(collection))
                        } else {
                            //create Error obj - throw unable to parse data error
                            let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                            completion(.failure(error))
                            
                        }
                    } else {
                        //create Error obj - throw unable to parse data error
                        let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                        completion (.failure(error))
                    }
                    
                
                case .failure(let afError) :
                    
                    let error = SevenPlusError.getSevenPlusErrorForUrlError(afError.underlyingError as? URLError, Constants.Error.errorDomain)
                    completion (.failure(error))
                }
            }
        } else {
            //create Error obj - throw network error
            let error = SevenPlusError.getNetworkError(Constants.Error.errorDomain)
            completion (.failure(error))
        }
    }
    
    // Fetch Movie With ID
    public func fetchMovieWithID(_ id: UInt,
                                 _ completion: @escaping (Result<Movie, SevenPlusError>) -> Void) {
        if self.isNetworkReachable {
            AF.request(Router.Movie(id)).response(queue: DispatchQueue.global(qos: .background)) { (response) in
                switch response.result {
                    
                case .success(_) :
                    
                    if let movieData = response.value ?? nil {
                        if let movie = Movie_JSONSerializer.generateMovieFrom(movieData) {
                            completion(.success(movie))
                        } else {
                            //create Error obj - throw unable to parse data error
                            let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                            completion(.failure(error))
                            
                        }
                    } else {
                        //create Error obj - throw unable to parse data error
                        let error = SevenPlusError.getDataError(Constants.Error.errorDomain)
                        completion (.failure(error))
                    }
                    
                
                case .failure(let afError) :
                    
                    let error = SevenPlusError.getSevenPlusErrorForUrlError(afError.underlyingError as? URLError, Constants.Error.errorDomain)
                    completion (.failure(error))
                }
            }
        } else {
            //create Error obj - throw network error
            let error = SevenPlusError.getNetworkError(Constants.Error.errorDomain)
            completion (.failure(error))
        }
    }
    
    //Fetch Image
    public func fetchImageFromURLString(_ width: String,
                                        _ urlString: String,
                                        _ completion: @escaping (Result<UIImage, SevenPlusError>) -> Void) {
        if self.isNetworkReachable {
            AF.request(Router.Image(width, urlString)).response(queue: DispatchQueue.global(qos: .background)) { response in
                 switch response.result {
                 
                 case .success(let responseData):
                    if let image = UIImage(data: responseData!, scale:1) {
                        completion(.success(image))
                    } else {
                        completion(.failure(SevenPlusError.getDefaultError(Constants.Error.errorDomain)))
                    }
                 
                 case .failure(let afError):
                    let error = SevenPlusError.getSevenPlusErrorForUrlError(afError.underlyingError as? URLError, Constants.Error.errorDomain)
                    completion(.failure(error))
                 }
             }
        } else {
            let error = SevenPlusError.getInternetNotReachableError(Constants.Error.errorDomain)
            completion(.failure(error))
        }
    }
}
