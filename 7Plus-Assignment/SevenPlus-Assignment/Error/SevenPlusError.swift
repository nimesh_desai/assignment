//
//  SevenPlusError.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

struct SevenPlusError: Error {
    let domain: String
    let code: Int
    let description: String
    
    fileprivate init(_ domain: String,
                     _ code: Int,
                     _ description: String) {
        self.domain = domain
        self.code = code
        self.description = description
    }
}

extension SevenPlusError {
    
    static func getNetworkError(_ domain: String) -> SevenPlusError {
        return SevenPlusError(domain,
                              Constants.networkErrorCode,
                              Constants.networkErrorDescription)
    }
    
    static func getInternetNotReachableError(_ domain: String) -> SevenPlusError {
        return SevenPlusError(domain,
                              Constants.noNetworkConnectionErrorCode,
                              Constants.noNetworkConnectionErrorDescription)
    }
    
    static func getDataError(_ domain: String) -> SevenPlusError {
        return SevenPlusError(domain,
                              Constants.dataErrorCode,
                              Constants.dataErrorDescription)
    }
    
    static func getDefaultError(_ domain: String) -> SevenPlusError {
        return SevenPlusError(domain,
                              Constants.networkErrorCode,
                              Constants.networkErrorDescription)
    }
    
    static func getSevenPlusErrorForUrlError(_ urlError: URLError?,
                                             _ errorDomain: String) -> SevenPlusError {
        if let error = urlError {
            switch error.code {
                case .cannotFindHost :
                    return SevenPlusError.getNetworkError(errorDomain)
                
                case .notConnectedToInternet:
                    return SevenPlusError.getInternetNotReachableError(errorDomain)
                
                default:
                    return SevenPlusError.getDefaultError(errorDomain)
            }
        }
        
        return SevenPlusError.getNetworkError(errorDomain)
    }
}
