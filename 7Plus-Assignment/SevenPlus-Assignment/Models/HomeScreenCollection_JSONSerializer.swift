//
//  HomeScreenCollection_JSONSerializer.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

class HomeScreenCollection_JSONSerializer {
    
    public static func generateHomeScreenCollectionFrom(_ jsonData: Data) -> HomeScreenCollection? {
        do {
            let collections = try JSONDecoder().decode(HomeScreenCollection.self,
                                                       from: jsonData)
            return collections
        } catch {
            return nil
        }
    }
}
