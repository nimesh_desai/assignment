//
//  HomeScreenCollection_SourceUrl.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

struct HomeScreenCollection_SourceUrl: Decodable {
    
    let title: String
    let url: String
    
    init(title: String,
         url: String) {
        self.title = title
        self.url = url
    }
    
    enum CodingKeys: String, CodingKey {
        case title
        case url
    }
}
