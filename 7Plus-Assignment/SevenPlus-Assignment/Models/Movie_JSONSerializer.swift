//
//  Movie_JSONSerializer.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation

class Movie_JSONSerializer {
    
    public static func generateMovieFrom(_ jsonData: Data) -> Movie? {
        do {
            let movie = try JSONDecoder().decode(Movie.self,
                                                 from: jsonData)
            return movie
        } catch {
            return nil
        }
    }
}
