//
//  HomeScreenCollection.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

struct HomeScreenCollection: Decodable {
    
    // Private vars
    private var items: [HomeScreenCollectionItem]
    
    // init
    init (items: [HomeScreenCollectionItem]) {
        self.items = items
    }
    
    // subscript conformance
    subscript(index: Int) -> HomeScreenCollectionItem {
        assert(indexIsValid(index: index), "Index out of range")
        return items[index]
    }
    
    // Public
    public var title: String?
    
    public var count: Int {
        get {
            return items.count
        }
    }
    
    public func numberOfItems() -> Int {
        return items.count
    }
    
    // Private
    private func indexIsValid(index: Int) -> Bool {
        return index >= 0 && index < items.count
    }
    
    enum CodingKeys: String, CodingKey {
        case items = "results"
    }
}
