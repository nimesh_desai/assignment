//
//  Movie.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

class Movie : DisplayableItem, Decodable, Displayable_Movie {
    
    let id: UInt
    let backdrop_image_path: String?
    let genres: [Genre]
    let overview: String
    let runtime: Int?
    let vote_average: Float
    let title: String
    
    init (id: UInt,
          backdrop_image_path: String? = nil,
          genres: [Genre],
          overview: String,
          runtime: Int? = nil,
          vote_average: Float,
          title: String) {
        self.id = id
        self.backdrop_image_path = backdrop_image_path
        self.genres = genres
        self.overview = overview
        self.runtime = runtime
        self.vote_average = vote_average
        self.title = title
    }
    
    override var imagePath: String? {
        return self.backdrop_image_path
    }

    enum CodingKeys: String, CodingKey {
        case id
        case backdrop_image_path = "backdrop_path"
        case genres
        case overview
        case runtime
        case vote_average
        case title
    }
}

extension Movie {
    
    // Displayable_Movie Conformance

    var displayableTitle: String {
        return self.title
    }
    
    var displayablGenre: String {
        
        if (self.genres.count > 1) {
            var stringToReturn = self.genres[0].name + Constants.comma
            for index in 1...self.genres.count-1 {
                if (index == self.genres.count - 1) {
                    stringToReturn += genres[index].name
                } else {
                    stringToReturn += genres[index].name + Constants.comma
                }
            }
            return stringToReturn
        } else if (self.genres.count == 1) {
            return self.genres[0].name
        } else {
            return Constants.defaultDisplayableText
        }
    }
    
    var displayableRunningTime: String {
        if var runtime = self.runtime {
            var hours = 0
            
            while (runtime >= Constants.minutesInAnHour) {
                hours += 1
                runtime -= Constants.minutesInAnHour
            }
            
            return String(hours) + Constants.hoursKey + " " + String(runtime) + Constants.minutesKey
        } else {
            return Constants.defaultDisplayableText
        }
        
    }
    
    var displayableVoteAverage: String {
        return String(self.vote_average) + Constants.voteAvgMax
    }
}
