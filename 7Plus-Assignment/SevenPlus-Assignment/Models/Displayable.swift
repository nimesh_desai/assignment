//
//  Displayable.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

enum ImageType {
    case poster
    case backdrop
}

protocol Displayable {
    func fetchImage(_ width: String,
                    _ completion: @escaping (_ image: UIImage?) -> Void)
    func reset_FetchImageCompletionHandler()
}
