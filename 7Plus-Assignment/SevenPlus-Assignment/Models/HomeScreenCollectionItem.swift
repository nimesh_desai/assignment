//
//  HomeScreenItem.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation
import UIKit

class HomeScreenCollectionItem: DisplayableItem, Decodable {
    
    let id: UInt
    let poster_path: String?
    
    init(id: UInt,
         poster_path: String? = nil) {
        self.id = id
        self.poster_path = poster_path
    }
    
    override var imagePath: String? {
        return self.poster_path
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case poster_path
    }
}
