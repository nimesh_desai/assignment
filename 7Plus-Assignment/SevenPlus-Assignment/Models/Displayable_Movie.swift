//
//  Displayable_Movie.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

protocol Displayable_Movie : Displayable {
    var displayableTitle: String { get }
    var displayablGenre: String { get }
    var displayableRunningTime: String { get }
    var displayableVoteAverage: String { get }
}
