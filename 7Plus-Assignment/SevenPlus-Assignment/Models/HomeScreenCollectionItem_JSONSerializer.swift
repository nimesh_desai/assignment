//
//  HomeScreenCollectionItemJSONSerializer.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

class HomeScreenCollectionItem_JSONSerializer {
    
    public static func generateHomeScreenCollectionItemFrom(_ jsonData: Data) -> HomeScreenCollectionItem? {
        do {
            let collectionItem = try JSONDecoder().decode(HomeScreenCollectionItem.self,
                                                          from: jsonData)
            return collectionItem
        } catch {
            return nil
        }
    }
}
