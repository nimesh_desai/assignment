//
//  Genre.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

struct Genre: Decodable, Equatable {
    
    let id: UInt
    let name: String
    
    init(id: UInt,
         name: String) {
        self.id = id
        self.name = name
    }
    
    internal static func ==(lhs: Genre, rhs: Genre) -> Bool {
        
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}
