//
//  DisplayableItem.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

class DisplayableItem : Displayable {
    
    enum ImageDownloadingState {
        case downloading
        case idle
    }
    
    var imagePath: String? {
        assert(false, "Override this property")
        return "NA"
    }
    
    var displayableImage: UIImage?
    private var imageDownloadingState: ImageDownloadingState = .idle
    private var imageDownlodedCompletionHandler:((UIImage?) -> Void)?
    
    // Private
    
    private func fetchImageFromServer(_ width: String,
                                      _ completion: @escaping (UIImage?) -> Void) {
        if (self.imageDownloadingState == .downloading) {
            return
        }
        
        guard let url = self.imagePath else {
            completion(nil)
            return
        }
        
        self.imageDownloadingState = .downloading
        weak var weakSelf = self
        
        let layer = ServiceLayer()
        layer.fetchImageFromURLString(width, url) { (result) in
            weakSelf?.imageDownloadingState = .idle
            
            switch result {
            case .success(let image):
                weakSelf?.displayableImage = image
                completion(image)
                
            case .failure(_):
                completion(nil)
            }
        }
    }
    
    // Displayable Conformance
    
    func fetchImage(_ width: String,
                    _ completion: @escaping (_ image: UIImage?) -> Void) {
        
        if let displayableImage = self.displayableImage {
            self.imageDownlodedCompletionHandler = nil
            completion(displayableImage)
        } else {
            self.imageDownlodedCompletionHandler = completion
            weak var weakSelf = self

            self.fetchImageFromServer (width) { (image) in
                
                weakSelf?.imageDownlodedCompletionHandler?(image)
                weakSelf?.imageDownlodedCompletionHandler = nil
            }
        }
    }
    
    func reset_FetchImageCompletionHandler() {
        self.imageDownlodedCompletionHandler = nil
    }
}
