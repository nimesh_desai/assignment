//
//  HomeScreenCollections_SourceUrl_JSONSerializer.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

class HomeScreenCollection_SourceUrl_JSONSerializer {
    
    public static func generateHomeScreenCollectionSourceUrlsFrom(_ jsonData: Data) -> [HomeScreenCollection_SourceUrl]? {
        do {
            let sourceUrls = try JSONDecoder().decode([HomeScreenCollection_SourceUrl].self,
                                                      from: jsonData)
            return sourceUrls
        } catch {
            return nil
        }
    }
}
