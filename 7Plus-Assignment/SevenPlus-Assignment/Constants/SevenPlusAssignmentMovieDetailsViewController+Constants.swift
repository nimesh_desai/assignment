//
//  SevenPlusAssignmentMovieDetailsViewController+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

extension SevenPlusAssignmentMovieDetailsViewController {
    
    enum BackdropImageSize: String {
        case w780
        case w1280
    }
    
    struct Constants {
        struct Error {
            static let errorDomain = "com.fifty.plus.seven.assignment.SevenPlusAssignmentMovieDetailsViewController"
        }
        
        static var backdropImageSizeForURL: String {
            get {
                if UIScreen.main.traitCollection.horizontalSizeClass == .regular {
                    return BackdropImageSize.w1280.rawValue
                } else {
                    return BackdropImageSize.w780.rawValue
                }
            }
        }
    }
}
