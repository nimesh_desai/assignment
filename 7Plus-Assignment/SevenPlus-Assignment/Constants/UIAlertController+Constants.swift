//
//  UIAlertController+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

extension (UIAlertController) {
    struct Constants {
        
        struct Alert {
            static let alertTitle = "Error"
            static let alertCancelButtonText = "OK"
            static let alertActionButtonText = "Retry"
        }
    }
}
