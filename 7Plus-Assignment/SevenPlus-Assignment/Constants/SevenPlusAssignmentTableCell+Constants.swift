//
//  SevenPlusAssignmentTableCell+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//
import UIKit

extension (SevenPlusAssignmentTableCell) {
    struct Constants {
        struct CollectionView {
            struct Cell {
                static let reuseIdentifier = "sevenPlusAssignmentCollectionCell"
                
                static var size: CGSize {
                    get {
                        if UIScreen.main.traitCollection.horizontalSizeClass == .regular {
                            return CGSize(width: 220, height: 220)
                        } else {
                            return CGSize(width: 140, height: 140)
                        }
                    }
                }
            }
        }
    }
}
