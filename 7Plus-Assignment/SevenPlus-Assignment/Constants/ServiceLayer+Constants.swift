//
//  ServiceLayer_Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

extension (ServiceLayer) {
    struct Constants {
        struct Error {
            static let errorDomain = "com.fifty.plus.seven.assignment.serviceLayer"
        }
    }
}
