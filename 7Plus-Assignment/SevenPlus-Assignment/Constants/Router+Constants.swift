//
//  Router+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation

extension (Router) {
    struct Constants {
        struct API {
            static let URL = "https://dl.dropboxusercontent.com/s/x8gtkmug5jn2cm4/7plus_home_screen.json"
            static let baseURL = "https://api.themoviedb.org/3/movie"
            static let imageBaseURL = "https://image.tmdb.org/t/p/"
        }
        
        struct Headers {
            static let Authorization_Key = "Authorization"
            static let Authorization_Value = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxZjE2YzNiOWIzOTM0MGExZTY4ZmExMTg0NDMxNmQzMSIsInN1YiI6IjYwMTEyYzgxZDdjZDA2MDAzYzA5NTUyMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.8w09ekRVnxYWsr4CHJDoT4Xc6AQVT9CxYEofHzo-fio"
            static let Content_Type_Key = "Content-Type"
            static let Content_Type_Value = "application/json"
        }
    }
}
