//
//  SevenPlusAssignmentHomeViewController+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//
import UIKit

extension (SevenPlusAssignmentHomeViewController) {
    
    struct Constants {
        
        struct TableView {
            static let numberOfItemsInASection = 1
            
            struct Cell {
                static let cellReuseIdentifier = "sevenPlusAssignmentTableCell"
                static var height: CGFloat {
                    get {
                        if UIScreen.main.traitCollection.horizontalSizeClass == .regular {
                            return 260.0
                        } else {
                            return 180.0
                        }
                    }
                }
            }
        }
        
        struct Navigation {
            static let navigationTitle = "7Plus"
        }
    }
}
