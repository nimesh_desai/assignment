//
//  DataSourceHelper+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

extension (DataSourceHelper) {
    struct Constants {
        struct Queue {
            static let serialQueue = "com.fifty.plus.seven.assignment.DataSourceHelper.sourceUrls.Queue"
            static let sourceUrlsName = "com.fifty.plus.seven.assignment.DataSourceHelper.sourceUrls.Queue"
            static let collectionsName = "com.fifty.plus.seven.assignment.DataSourceHelper.collections.Queue"
        }
    }
}
