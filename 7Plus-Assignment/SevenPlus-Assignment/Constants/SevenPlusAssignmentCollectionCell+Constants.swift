//
//  SevenPlusAssignmentCollectionCell+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//
import Foundation
import UIKit

extension (SevenPlusAssignmentCollectionCell) {
    
    enum PosterImageSize: String {
        case w154
        case w342
    }
    
    struct Constants {
        static let cornerRadius: CGFloat = 8
        static let borderColor = UIColor.black.cgColor
        static let borderWidth: CGFloat = 1.0
        
        static var posterImageSizeForURL: String {
            get {
                if UIScreen.main.traitCollection.horizontalSizeClass == .regular {
                    return PosterImageSize.w342.rawValue
                } else {
                    return PosterImageSize.w154.rawValue
                }
            }
        }
    }
}
