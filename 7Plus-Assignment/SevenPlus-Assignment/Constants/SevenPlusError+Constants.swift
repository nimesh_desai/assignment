//
//  SevenPlusError+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

extension (SevenPlusError) {

    struct Constants {
        static let networkErrorCode = 4001
        static let networkErrorDescription = "An error occured while trying to download data. Please try again."
        static let noNetworkConnectionErrorCode = 4002
        static let noNetworkConnectionErrorDescription = "Could not connect to the servers. Please check your internet connection and try again."
        static let dataErrorCode = 4003
        static let dataErrorDescription = "An error occured while trying to parse your data. Please try again."
    }
}
