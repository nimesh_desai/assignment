//
//  Movie+Constants.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation

extension Movie {
    struct Constants {
        static let defaultDisplayableText = "NA"
        static let hoursKey = "h"
        static let minutesKey = "m"
        static let minutesInAnHour = 60
        static let comma = ", "
        static let voteAvgMax = " / 10"
    }
}
