//
//  DataSourceHelper.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import Foundation

class DataSourceHelper {
    
    private var dispatchQueue = DispatchQueue(label: Constants.Queue.serialQueue,
                                              qos: .background)
    private var dispatchQueueSourceUrls = DispatchQueue(label: Constants.Queue.sourceUrlsName,
                                                        qos: .background)
    private var dispatchQueueCollections = DispatchQueue(label: Constants.Queue.collectionsName,
                                                         qos: .background)
    
    // Public
    public func fetch_homescreen_data(_ completion: @escaping (Result<[HomeScreenCollection], SevenPlusError>) -> Void) {
        self.dispatchQueueSourceUrls.async {
            self.fetch_homescreen_collection_source_urls { (result) in
                switch (result) {
                
                case.success(let sourceUrls):
                    self.fetch_collections_from_sourceUrls(sourceUrls) { (result) in
                        switch (result) {
                        case .success(let collections):
                            completion(.success(collections))
                            
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    public func fetch_movieWithID(_ movieID:UInt , completion: @escaping (Result<Movie, SevenPlusError>) -> Void) {
        self.dispatchQueue.async {
            let layer = ServiceLayer()
            layer.fetchMovieWithID(movieID) { (result) in
                
                self.dispatchQueue.async {
                    switch (result) {
                    case .success(let movie):
                        completion(.success(movie))
                        
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        }
    }
    
    // Private
    
    private func fetch_homescreen_collection_source_urls(_ completion: @escaping (Result<[HomeScreenCollection_SourceUrl], SevenPlusError>) -> Void) {
        
        let layer = ServiceLayer()
        layer.fetch_homescreen_collection_urls { (result) in
            
            switch (result) {
            case .success(let sourceUrls):
                completion(.success(sourceUrls))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func fetch_collections_from_sourceUrls(_ collectionSourceUrls: [HomeScreenCollection_SourceUrl],
                                                   _ completion: @escaping (Result<[HomeScreenCollection], SevenPlusError>) -> Void) {
        
        self.dispatchQueueCollections.async {
            var homeScreenCollections = [HomeScreenCollection]()
            var maxUrls = collectionSourceUrls.count
            
            for sourceUrl in collectionSourceUrls {
                
                self.fetch_collection_from_url(sourceUrl.url,
                                               sourceUrl.title) { (result) in
                    
                    switch (result) {
                    
                    case .success(let homeScreenCollection):
                        
                        homeScreenCollections.append(homeScreenCollection)
                        maxUrls -= 1
                        
                        if (maxUrls <= 0) {
                            completion(.success(homeScreenCollections))
                        }
                        
                    case .failure(_):
                        print(homeScreenCollections.count)
                        maxUrls -= 1
                        
                        if (maxUrls <= 0) {
                            completion(.success(homeScreenCollections))
                        }
                    }
                }
            }
        }
    }
    
    private func fetch_collection_from_url(_ url: String,
                                           _ title: String,
                                           _ completion: @escaping (Result<HomeScreenCollection, SevenPlusError>) -> Void) {
        
        self.dispatchQueueCollections.async {
            
            let layer = ServiceLayer()
            layer.fetch_homescreen_collection(url,
                                              title) { (result) in
                self.dispatchQueueCollections.async {
                    
                    switch(result) {
                    case .success(let collection):
                        completion(.success(collection))
                        
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        }
    }
}
