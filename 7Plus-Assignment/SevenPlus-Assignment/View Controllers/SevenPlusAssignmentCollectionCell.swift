//
//  SevenPlusAssignmentCollectionCell.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

class SevenPlusAssignmentCollectionCell: UICollectionViewCell {

    // IBOutlets
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var moviePosterImageView: UIImageView!
    
    // Private var
    private var dataSource: Displayable?
    
    // Init
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.setupCellBorders()
    }
    
    // Overrides
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.resetDataSource()
        self.moviePosterImageView.image = nil
        self.moviePosterImageView.isHidden = true
        self.activityIndicator.startAnimating()
    }
    
    // Public
    func updateCellWithCollectionItem(collectionItem: Displayable) {
        
        self.resetDataSource()
        
        self.dataSource = collectionItem
        
        self.dataSource?.fetchImage(SevenPlusAssignmentCollectionCell.Constants.posterImageSizeForURL) { [weak self] (image) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async {
                self.moviePosterImageView.image = image
                if (image != nil) {
                    self.moviePosterImageView.isHidden = false
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    //Private
    private func setupCellBorders() {
        self.layer.cornerRadius = Constants.cornerRadius
        self.layer.masksToBounds = true
        self.layer.borderColor = Constants.borderColor
        self.layer.borderWidth = Constants.borderWidth
    }
    
    private func resetDataSource() {
        if let dataSource = self.dataSource {
            dataSource.reset_FetchImageCompletionHandler()
            self.dataSource = nil
        }
    }
}
