//
//  Presenter.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import UIKit

class Presenter {
    
    static func presentMovieDetailsController(for movieID:UInt,
                                              from viewController: UIViewController) {
        let movieDetailsViewController = ViewControllerFactory.createMovieDetailsViewController(movieID)
        viewController.navigationController?.pushViewController(movieDetailsViewController,
                                                                animated: true)
    }
}
