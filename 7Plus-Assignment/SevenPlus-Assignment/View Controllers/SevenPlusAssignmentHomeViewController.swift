//
//  SevenPlusAssignmentHomeViewController.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import UIKit

class SevenPlusAssignmentHomeViewController: SevenPlusAssignmentViewController, UITableViewDelegate, UITableViewDataSource {
    
    //IBOutlets
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    
    //iVars
    private var homescreenCollections:[HomeScreenCollection]? {
        didSet {
            self.stopRefreshAnimation()
            self.tableView.reloadData()
        }
    }

    // Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    // Initial Setup
    private func initialSetup() {
        //hide table view
        self.tableView.isHidden = true
        
        //start activity indicator
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        //setup navigation bar
        self.setupNavigatorBar()
        
        //setup pull to refresh
        self.setupPullToRefresh()
        
        //fetch data
        self.refreshData(nil)
    }
    
    // Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.TableView.Cell.height
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.homescreenCollections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let collections = self.homescreenCollections {
            let collection = collections[section]
            return collection.title
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.TableView.numberOfItemsInASection
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Table View DataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: Constants.TableView.Cell.cellReuseIdentifier,
                                                      for: indexPath) as! SevenPlusAssignmentTableCell
        if let collections = self.homescreenCollections  {
            if indexPath.section < collections.count {
                let collection = collections[indexPath.section]
                cell.updateCellWithHomeScreenCollection(collection) { (movieID) in
                    // navigate to movie page for movieID
                    self.navigateToMovieWithID(movieID)
                }
            }
        }
        
        return cell
    }
    
    // Private
    
    // Setup Navigation Bar
    private func setupNavigatorBar() {
        self.title = Constants.Navigation.navigationTitle
    }
    
    // Setup Pull To Refresh
    private func setupPullToRefresh() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(refreshData(_:)),
                                 for: .valueChanged)

        self.tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshData(_ sender: Any?) {
        let dataSourceHelper = DataSourceHelper()
        
        dataSourceHelper.fetch_homescreen_data { (result) in
            switch (result) {
            case .success(let collections):
                DispatchQueue.main.async {
                    self.homescreenCollections = collections
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.showAlertForError(error)
                }
            }
        }
    }
    
    // Stop Refreshing
    private func stopRefreshAnimation() {
        self.tableView.refreshControl?.endRefreshing()
        UIView.animate(withDuration: 0.01) {
            self.tableView.contentOffset = CGPoint.zero
        }
        self.activityIndicator.stopAnimating()
        self.tableView.isHidden = false
    }
    
    // Show Alert
    private func showAlertForError(_ error: SevenPlusError) {
        
        DispatchQueue.main.async {
            self.stopRefreshAnimation()
            
            self.showAlert(for: error) {
                
                self.activityIndicator.startAnimating()
                self.refreshData(nil)
            }
        }
    }
    
    // Navigate to Movie
    private func navigateToMovieWithID(_ movieID: UInt) {
        Presenter.presentMovieDetailsController(for: movieID,
                                                from: self)
    }
}
