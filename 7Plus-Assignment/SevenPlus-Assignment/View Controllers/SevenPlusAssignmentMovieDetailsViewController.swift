//
//  SevenPlusAssignmentMovieDetailsViewController.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import UIKit

class SevenPlusAssignmentMovieDetailsViewController: SevenPlusAssignmentViewController {

    // IBOutlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleValueLabel: UILabel!
    @IBOutlet weak var movieGenreValueLabel: UILabel!
    @IBOutlet weak var movieRunningTimeValueLabel: UILabel!
    @IBOutlet weak var movieAverageRatingValueLabel: UILabel!
    
    // iVar
    var movieID: UInt?
    
    private var displayableMovie: Displayable_Movie? {
        didSet {
            DispatchQueue.main.async {
                self.updateViewsWithData()
            }
        }
    }
    
    // Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let displayableMovie = self.displayableMovie {
            displayableMovie.reset_FetchImageCompletionHandler()
        }
    }
    
    // Private
    
    private func fetchData() {
        if let movieID = self.movieID {
            let datasourceHelper = DataSourceHelper()
            
            datasourceHelper.fetch_movieWithID(movieID) { (result) in
                switch (result) {
                case .success(let movie):
                    self.displayableMovie = movie
                    
                case .failure(let error):
                    self.showAlertForError(error)
                }
            }
        } else {
            self.showAlertForError(SevenPlusError.getDefaultError(Constants.Error.errorDomain))
        }
    }
    
    // Show Alert
    private func showAlertForError(_ error: SevenPlusError) {
        
        DispatchQueue.main.async {
            self.showAlert(for: error) {
                self.fetchData()
            }
        }
    }
    
    // Update Views With Data
    private func updateViewsWithData() {
        if let displayableMovie = self.displayableMovie {
            self.title = displayableMovie.displayableTitle
            self.movieTitleValueLabel.text = displayableMovie.displayableTitle
            self.movieGenreValueLabel.text = displayableMovie.displayablGenre
            self.movieRunningTimeValueLabel.text = displayableMovie.displayableRunningTime
            self.movieAverageRatingValueLabel.text = displayableMovie.displayableVoteAverage
            
            displayableMovie.fetchImage(Constants.backdropImageSizeForURL) { [weak self] (image) in
                guard let self = self else {
                    return
                }
                
                DispatchQueue.main.async {
                    self.movieImageView.image = image
                    if (image != nil) {
                        self.movieImageView.isHidden = false
                        self.activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
}
