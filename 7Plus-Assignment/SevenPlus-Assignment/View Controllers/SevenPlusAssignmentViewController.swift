//
//  SevenPlusAssignmentViewController.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 29/1/21.
//

import UIKit

class SevenPlusAssignmentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    public func showAlert(for error: SevenPlusError,
                          withRetryButtonAction retryAction: @escaping (() -> Void)) {
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: UIAlertController.Constants.Alert.alertTitle,
                                          message: error.description,
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: UIAlertController.Constants.Alert.alertCancelButtonText,
                                             style: .cancel,
                                             handler: { action in
            })
            
            let retryAction = UIAlertAction(title: UIAlertController.Constants.Alert.alertActionButtonText,
                                            style: .default,
                                            handler: { action in
                                                retryAction()
            })
            
            alert.addAction(cancelAction)
            alert.addAction(retryAction)
        
            self.present(alert,
                         animated: true,
                         completion: nil)
        }
    }
}
