//
//  ViewControllerFactory.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//
import UIKit

class ViewControllerFactory {
    
    static func createMovieDetailsViewController(_ movieID: UInt) -> SevenPlusAssignmentMovieDetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let movieDetailsViewController = storyboard.instantiateViewController(withIdentifier: "SevenPlusAssignmentMovieDetailsViewController") as! SevenPlusAssignmentMovieDetailsViewController
        
        movieDetailsViewController.movieID = movieID
        
        return movieDetailsViewController
    }
}
