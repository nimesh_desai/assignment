//
//  SevenPlusAssignmentTableCell.swift
//  SevenPlus-Assignment
//
//  Created by Nimesh 'Fifty' Desai on 28/1/21.
//

import Foundation
import UIKit

class SevenPlusAssignmentTableCell : UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //iVars
    private var homescreenCollection: HomeScreenCollection? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    private var selectionCallback: ((UInt) -> Void)?
    
    // IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // Overrides
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetDataSource()
    }
    
    // Collection View Conformance
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homescreenCollection?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionView.Cell.reuseIdentifier,
                                                           for: indexPath) as! SevenPlusAssignmentCollectionCell
        
        if let collection = self.homescreenCollection {
            let collectionItem = collection[indexPath.item]
            cell.updateCellWithCollectionItem(collectionItem: collectionItem)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let collection = self.homescreenCollection {
            let collectionItem = collection[indexPath.item]
            
            if let selectionCallback = self.selectionCallback {
                selectionCallback(collectionItem.id)
            }
        }
    }
    
    // Collection View Flow Delegate Conformance
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Constants.CollectionView.Cell.size
    }
    
    // Public
    
    public func updateCellWithHomeScreenCollection(_ collection: HomeScreenCollection,
                                                   _ selectionCallback: @escaping (UInt) -> Void) {
        self.homescreenCollection = collection
        self.selectionCallback = selectionCallback
    }
    
    // Private
    private func resetDataSource() {
        self.homescreenCollection = nil
        self.selectionCallback = nil
    }
}
