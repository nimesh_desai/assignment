//
//  Displayable_Movie_Tests.swift
//  SevenPlus-AssignmentTests
//
//  Created by Nimesh 'Fifty' Desai on 29/1/21.
//

import XCTest

class Displayable_Movie_Tests: XCTestCase {

    func test_displayable_movie_expected_title() {
        guard let data = self.getJSONDataFromFile("valid_one_movie") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedDisplayableTitle = "Wonder Woman 1984"
        
        XCTAssertEqual(movie.displayableTitle,
                       expectedDisplayableTitle,
                       "expectedDisplayableTitle is: \(expectedDisplayableTitle), but got: \(movie.displayableTitle)")
    }
    
    func test_displayable_movie_expected_runtime() {
        guard let data = self.getJSONDataFromFile("valid_one_movie") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedDisplayableRunningTime = "2h 31m"
        
        XCTAssertEqual(movie.displayableRunningTime,
                       expectedDisplayableRunningTime,
                       "Expected expectedDisplayableRunningTime is: \(expectedDisplayableRunningTime), but got: \(movie.displayableRunningTime)")
        
    }
    
    func test_displayable_movie_nil_runtime() {
        guard let data = self.getJSONDataFromFile("valid_one_movie_nil_runtime") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedDisplayableRunningTime = "NA"
        
        XCTAssertEqual(movie.displayableRunningTime,
                       expectedDisplayableRunningTime,
                       "Expected expectedDisplayableRunningTime is: \(expectedDisplayableRunningTime), but got: \(movie.displayableRunningTime)")
        
    }
    
    func test_displayable_movie_expected_genre() {
        guard let data = self.getJSONDataFromFile("valid_one_movie_nil_runtime") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedDisplayablGenre = "Fantasy, Action, Adventure"
        
        XCTAssertEqual(movie.displayablGenre,
                       expectedDisplayablGenre,
                       "Expected expectedDisplayablGenre is: \(expectedDisplayablGenre), but got: \(movie.displayablGenre)")
    }
    
    func test_displayable_movie_expected_vote_average() {
        guard let data = self.getJSONDataFromFile("valid_one_movie_nil_runtime") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedDisplayableVoteAverage = "7.1"
        
        XCTAssertEqual(movie.displayableVoteAverage,
                       expectedDisplayableVoteAverage,
                       "Expected expectedDisplayableVoteAverage is: \(expectedDisplayableVoteAverage), but got: \(movie.displayableVoteAverage)")
    }
    
    // Get JSON Data
    private func getJSONDataFromFile(_ file: String) -> Data? {
        let testBundle = Bundle(for: type(of: self))
        
        if let path = testBundle.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
              } catch {
                return nil
              }
        }
        
        return nil
    }
}
