//
//  Movie_JSONSerializer_Tests.swift
//  SevenPlus-AssignmentTests
//
//  Created by Nimesh 'Fifty' Desai on 27/1/21.
//

import XCTest

class Movie_JSONSerializer_Tests: XCTestCase {
    
    func test_valid_one_movie() {
        guard let data = self.getJSONDataFromFile("valid_one_movie") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedID:UInt = 464052
        let expectedBackdropImagePath = "/srYya1ZlI97Au4jUYAktDe3avyA.jpg"
        let expectedGenres = [Genre(id: 14, name: "Fantasy"),
                              Genre(id: 28, name: "Action"),
                              Genre(id: 12, name: "Adventure")]
        let expectedOverview = "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah."
        let expectedRuntime = 151
        let expectedVoteAverage:Float = 7.1
        let expectedTitle = "Wonder Woman 1984"
        
        XCTAssertEqual(movie.id,
                       expectedID,
                       "Expected ID is: \(expectedID), but got: \(movie.id)")
        XCTAssertEqual(movie.backdrop_image_path,
                       expectedBackdropImagePath,
                       "Expected Backdrop path is: \(expectedBackdropImagePath), but got: \(movie.backdrop_image_path ?? "na")")
        XCTAssertEqual(movie.genres,
                       expectedGenres,
                       "Expected Genres are: \(expectedGenres), but got: \(movie.genres)")
        XCTAssertEqual(movie.overview,
                       expectedOverview,
                       "Expected Overview is: \(expectedOverview), but got: \(movie.overview)")
        XCTAssertEqual(movie.runtime,
                       expectedRuntime,
                       "Expected Runtime is: \(expectedRuntime), but got: \(movie.runtime ?? 0)")
        XCTAssertEqual(movie.vote_average,
                       expectedVoteAverage,
                       "Expected Vote Average is: \(expectedVoteAverage), but got: \(movie.vote_average)")
        XCTAssertEqual(movie.title,
                       expectedTitle,
                       "Expected Title is: \(expectedTitle), but got: \(movie.title)")
    }
    
    func test_valid_one_movie_backdrop_image_path_nil() {
        
        guard let data = self.getJSONDataFromFile("valid_one_movie_backdrop_image_path_nil") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedID:UInt = 464052
        let expectedGenres = [Genre(id: 14, name: "Fantasy"),
                              Genre(id: 28, name: "Action"),
                              Genre(id: 12, name: "Adventure")]
        let expectedOverview = "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah."
        let expectedRuntime = 151
        let expectedVoteAverage:Float = 7.1
        let expectedTitle = "Wonder Woman 1984"
        
        XCTAssertEqual(movie.id,
                       expectedID,
                       "Expected ID is: \(expectedID), but got: \(movie.id)")
        XCTAssertEqual(movie.backdrop_image_path,
                       nil,
                       "Expected Backdrop path is nil, but got: \(movie.backdrop_image_path ?? "na")")
        XCTAssertEqual(movie.genres,
                       expectedGenres,
                       "Expected Genres are: \(expectedGenres), but got: \(movie.genres)")
        XCTAssertEqual(movie.overview,
                       expectedOverview,
                       "Expected Overview is: \(expectedOverview), but got: \(movie.overview)")
        XCTAssertEqual(movie.runtime,
                       expectedRuntime,
                       "Expected Runtime is: \(expectedRuntime), but got: \(movie.runtime ?? 0)")
        XCTAssertEqual(movie.vote_average,
                       expectedVoteAverage,
                       "Expected Vote Average is: \(expectedVoteAverage), but got: \(movie.vote_average)")
        XCTAssertEqual(movie.title,
                       expectedTitle,
                       "Expected Title is: \(expectedTitle), but got: \(movie.title)")
    }
    
    func test_valid_one_movie_runtime_nil() {
        guard let data = self.getJSONDataFromFile("valid_one_movie_nil_runtime") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedID:UInt = 464052
        let expectedBackdropImagePath = "/srYya1ZlI97Au4jUYAktDe3avyA.jpg"
        let expectedGenres = [Genre(id: 14, name: "Fantasy"),
                              Genre(id: 28, name: "Action"),
                              Genre(id: 12, name: "Adventure")]
        let expectedOverview = "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah."
        let expectedVoteAverage:Float = 7.1
        let expectedTitle = "Wonder Woman 1984"
        
        XCTAssertEqual(movie.id,
                       expectedID,
                       "Expected ID is: \(expectedID), but got: \(movie.id)")
        XCTAssertEqual(movie.backdrop_image_path,
                       expectedBackdropImagePath,
                       "Expected Backdrop path is: \(expectedBackdropImagePath), but got: \(movie.backdrop_image_path ?? "na")")
        XCTAssertEqual(movie.genres,
                       expectedGenres,
                       "Expected Genres are: \(expectedGenres), but got: \(movie.genres)")
        XCTAssertEqual(movie.overview,
                       expectedOverview,
                       "Expected Overview is: \(expectedOverview), but got: \(movie.overview)")
        XCTAssertEqual(movie.runtime,
                       nil,
                       "Expected Runtime is nil, but got: \(movie.runtime ?? -1)")
        XCTAssertEqual(movie.vote_average,
                       expectedVoteAverage,
                       "Expected Vote Average is: \(expectedVoteAverage), but got: \(movie.vote_average)")
        XCTAssertEqual(movie.title,
                       expectedTitle,
                       "Expected Title is: \(expectedTitle), but got: \(movie.title)")
    }
    
    func test_valid_one_movie_both_backdrop_path_runtime_nil() {
        guard let data = self.getJSONDataFromFile("valid_one_movie_both_backdrop_image_path_runtime_nil") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let movie = Movie_JSONSerializer.generateMovieFrom(data) else {
            XCTFail("movie should not be nil")
            return
        }
        
        let expectedID:UInt = 464052
        let expectedGenres = [Genre(id: 14, name: "Fantasy"),
                              Genre(id: 28, name: "Action"),
                              Genre(id: 12, name: "Adventure")]
        let expectedOverview = "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah."
        let expectedVoteAverage:Float = 7.1
        let expectedTitle = "Wonder Woman 1984"
        
        XCTAssertEqual(movie.id,
                       expectedID,
                       "Expected ID is: \(expectedID), but got: \(movie.id)")
        XCTAssertEqual(movie.backdrop_image_path,
                       nil,
                       "Expected Backdrop path is: nil, but got: \(movie.backdrop_image_path ?? "na")")
        XCTAssertEqual(movie.genres,
                       expectedGenres,
                       "Expected Genres are: \(expectedGenres), but got: \(movie.genres)")
        XCTAssertEqual(movie.overview,
                       expectedOverview,
                       "Expected Overview is: \(expectedOverview), but got: \(movie.overview)")
        XCTAssertEqual(movie.runtime,
                       nil,
                       "Expected Runtime is: nil, but got: \(movie.runtime ?? -1)")
        XCTAssertEqual(movie.vote_average,
                       expectedVoteAverage,
                       "Expected Vote Average is: \(expectedVoteAverage), but got: \(movie.vote_average)")
        XCTAssertEqual(movie.title,
                       expectedTitle,
                       "Expected Title is: \(expectedTitle), but got: \(movie.title)")
    }
    
    // Get JSON Data
    private func getJSONDataFromFile(_ file: String) -> Data? {
        let testBundle = Bundle(for: type(of: self))
        
        if let path = testBundle.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
              } catch {
                return nil
              }
        }
        
        return nil
    }
}
